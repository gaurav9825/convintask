from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from datetime import datetime


class Store(models.Model):
    name = models.CharField( max_length=120)
    file = models.FileField(upload_to='uploads/')


@receiver(pre_save, sender=Store)
def do_something_if_changed(sender, instance, **kwargs):
    try:
        obj = sender.objects.get(pk=instance.pk)
    except sender.DoesNotExist:
        pass # Object is new, so field hasn't technically changed, but you may want to do something else here.
    else:
        newname = str(instance.file.name).split('.')[0]
        oldname = str(obj.file.name[8:]).split('.')[0]
        ans = oldname.startswith(newname,0)
        if not obj.name == instance.name: 
            with open("changelog.txt", 'a') as out:
              out.write(f"Old name value: {obj.name}, New name value: {instance.name},  Timestamp: {datetime.now()}" + '\n')
        if not ans:
            with open("changelog.txt", 'a') as out:
              out.write(f"Old File name: {oldname}, New File value: {newname},  Timestamp: {datetime.now()}" + '\n') 
